## OFFICIAL WEBSITE OF NK "MLADOST" CACINCI

Getting started

Make sure you have:

1. Proper Ruby installed (check .ruby-version)
2. PostgreSQL database installed
3. Bundler gem installed (gem install bundler)
4. Foreman gem installed (gem install foreman)

Then do following:

~~~~
git clone git@bitbucket.org:rama19/nkmladostcacinci.git
cd nkmladostcacinci
bundle install
cp env.example .env # See [1] for more details
rake db:create
rake db:schema:load
foreman start
open http://localhost:7000
~~~~

[1] Enter/update admin credentials and Cloudinary secrets

### Repository

  Code is on [BitBucket](https://bitbucket.org/rama19/nkmladostcacinci)

### Deploy

  To deploy to heroku use `git push heroku master`

