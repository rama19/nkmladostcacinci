class SetDefaultValueToGoalDifferenceInChampionships < ActiveRecord::Migration
  def up
    change_column :championships, :goal_difference, :string, default: "0 - 0"
  end

  def down
    change_column :championships, :goal_difference, :string
  end
end
