class AddPublishedOnToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :published_on, :date

    Article.find_each do |article|
      article.update(published_on: article.created_at)
    end
  end
end
