class CreateChampionships < ActiveRecord::Migration
  def change
    create_table :championships do |t|
      t.string :team
      t.integer :games_played, default: 0
      t.integer :wins, default: 0
      t.integer :draws, default: 0
      t.integer :lost, default: 0
      t.string :goal_difference
      t.integer :points, default: 0

      t.timestamps null: false
    end
  end
end
