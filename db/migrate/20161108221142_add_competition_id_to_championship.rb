class AddCompetitionIdToChampionship < ActiveRecord::Migration
  def change
    add_column :championships, :competition_id, :integer
  end
end
