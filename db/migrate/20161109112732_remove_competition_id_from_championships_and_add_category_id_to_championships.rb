class RemoveCompetitionIdFromChampionshipsAndAddCategoryIdToChampionships < ActiveRecord::Migration
  def change
    remove_column :championships, :competition_id, :integer

    add_column :championships, :category_id, :integer
  end
end
