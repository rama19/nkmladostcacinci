class RemoveCompetitionIdFromChampionships < ActiveRecord::Migration
  def change
    remove_column :championships, :competition_id
  end
end
