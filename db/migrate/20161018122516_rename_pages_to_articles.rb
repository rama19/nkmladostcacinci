class RenamePagesToArticles < ActiveRecord::Migration
  def change
    rename_table :pages, :articles
  end
end
