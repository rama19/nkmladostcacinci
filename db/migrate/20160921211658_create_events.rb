class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :competition
      t.string :matchday
      t.string :place
      t.date :game_date
      t.string :game_time
      t.string :home_team
      t.string :away_team

      t.timestamps null: false
    end
  end
end
