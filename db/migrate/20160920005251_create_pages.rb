class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :short_description
      t.text :body
      t.string :slug
      t.boolean :published

      t.timestamps null: false
    end
  end
end
