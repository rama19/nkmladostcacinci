# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.delete_all

if Rails.env.development?
  [
    {email: "matijaramic@gmail.com", first_name: "Matija", last_name: "Ramić"},
    {email: "alen.jurenac@gmail.com", first_name: "Alen", last_name: "Jurenac"},
    {email: "nkmladostcacinci@gmail.com", first_name: "Saša", last_name: "Tir"}
  ].each do |user|
    User.create!(user.merge(password: "nkmladost1929"))
  end
end
