class Admin::BaseController < ApplicationController

  layout "admin"

  before_action :require_login

  # before_action :authenticate_admin!
  #
  # private
  #
  # def authenticate_admin!
  #   authenticate_or_request_with_http_basic do |username, password|
  #     username == ENV.fetch("ADMIN_USERNAME") && password == ENV.fetch("ADMIN_PASSWORD")
  #   end
  # end
end
