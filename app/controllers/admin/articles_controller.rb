class Admin::ArticlesController < Admin::BaseController

  def index
    @articles = Article.all.page(params[:page]).per_page(20)
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to admin_articles_path, notice: "Članak kreiran!"
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to admin_articles_path, notice: "Izmjene uspješno spremljene!"
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to admin_articles_path
  end


  private

  def article_params
    params.require(:article).permit(:title, :image, :short_description, :body, :slug, :published, :published_on)
  end
end
