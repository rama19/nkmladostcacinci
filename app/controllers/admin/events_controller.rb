class Admin::EventsController < Admin::BaseController

  def index
    @events = Event.all.page(params[:page]).per_page(20)
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to admin_events_path, notice: "Događaj uspješno kreiran!"
    else
      render 'new'
    end
  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      redirect_to admin_events_path, notice: "Događaj uspješno spremljen!"
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    redirect_to admin_events_path
  end

  private

  def event_params
    params.require(:event).permit(:matchday, :place, :game_date, :game_time, :home_team,
                                  :home, :away_team, :away, :competition_id, :category_id)
  end

end
