class Admin::CategoriesController < Admin::BaseController

  def index
    @categories = Category.all.page(params[:page]).per_page(20)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to admin_categories_path, notice: "Uspješno kreirano!"
    else
      render 'new'
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])

    if @category.update(category_params)
      redirect_to admin_categories_path, notice: "Izmjene uspješno spremljene!"
    else
      render 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    redirect_to admin_categories_path
  end


  private

  def category_params
    params.require(:category).permit(:name)
  end
end
