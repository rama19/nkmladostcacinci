class Admin::InterfaceController < Admin::BaseController

  def index
    @articles = Article.all
    @events = Event.all
    @categories = Category.all
  end
end
