class Admin::CompetitionsController < Admin::BaseController

  def index
    @competitions = Competition.all.page(params[:page]).per_page(20)
  end

  def new
    @competition = Competition.new
  end

  def create
    @competition = Competition.new(competition_params)

    if @competition.save
      redirect_to admin_competitions_path, success: "Natjecanje kreirano!"
    else
      render 'new'
    end
  end

  def edit
    @competition = Competition.find(params[:id])
  end

  def update
    @competition = Competition.find(params[:id])

    if @competition.update(competition_params)
      redirect_to admin_competitions_path, notice: "Izmjene uspješno spremljene!"
    else
      render 'edit'
    end
  end

  def destroy
    @competition = Competition.find(params[:id])
    @competition.destroy

    redirect_to admin_competitions_path, notice: "Natjecanje izbrisano!"
  end

  private

  def competition_params
    params.require(:competition).permit(:name)
  end

end
