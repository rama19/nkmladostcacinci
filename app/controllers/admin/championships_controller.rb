class Admin::ChampionshipsController < Admin::BaseController

  def index
  end

  def new
    @championship = Championship.new
  end

  def create
    @championship = Championship.new(championship_params)

    if @championship.save
      redirect_to admin_championships_path, notice: "Uspješno kreirano!"
    else
      render 'new'
    end
  end

  def edit
    @championship = Championship.find(params[:id])
  end

  def update
    @championship = Championship.find(params[:id])

    respond_to do |format|
      if @championship.update(championship_params)
        format.html { redirect_to(admin_championships_path, notice: "Izmjene spremljenje!")}
      else
        format.html { render "edit" }
      end
    end
  end

  def destroy
    @championship = Championship.find(params[:id])
    @championship.destroy

    redirect_to admin_championships_path
  end


  private

  def championship_params
    params.require(:championship).permit(:team, :games_played, :wins, :draws, :lost, :goal_difference, :points, :category_id)
  end

end
