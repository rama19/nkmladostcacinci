class Admin::ImagesController < Admin::BaseController

  def index
    @images = Image.order("created_at DESC").page(params[:page]).per_page(8)
  end

  def create
    @image = Image.new(image_params)
    if @image.save
      render status: :created
    else
      render status: :bad_request
    end
  end

  private

  def image_params
    params.require(:image).permit(:attachment)
  end
end
