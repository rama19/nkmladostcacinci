class SiteController < ApplicationController

  def home
    @articles = Article.all.order("published_on DESC").limit(1)
  end

  def blog
    @articles = Article.all.order("published_on DESC").page(params[:page]).per_page(6)
  end

  def show
    @article = Article.find_by_slug!(params[:slug])
  end

end
