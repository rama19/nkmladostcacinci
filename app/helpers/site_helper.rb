module SiteHelper

  def link_to_article(article, text: nil)
    text ||= article.title
    link_to text, "/site/#{article.slug}"
  end

  def main_menu_items
    [
      ["Početna", root_path],
      ["Novosti", blog_path],
      ["Momčad", team_path],
      ["Povijest", history_path]
    ]
  end

  def render_main_menu
    items = main_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      content_tag(:li, link_to(item[0], item[1]), class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "navigation")
  end

  def md_to_html(text)
    options = {
      hard_wrap: true,
      fenced_code_blocks: true,
      gh_blockcode: true
    }
    extensions = {
      autolink: true,
      no_intra_emphasis: true
    }
    renderer = Redcarpet::Render::HTML.new(options)
    md_for_html= Redcarpet::Markdown.new(renderer, extensions)
    md_for_html.render(text).html_safe
  end

  def team_menu_items
    [
      ["Seniori", team_path],
      ["Juniori", juniors_path],
      ["Pioniri", pionirs_path],
      ["U-12", u12_path],
      ["U-10", u10_path],
      ["U-8", u8_path]
    ]
  end

  def render_team_menu
    items = team_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      content_tag(:li, link_to(item[0], item[1]), class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "team-nav")
  end
end
