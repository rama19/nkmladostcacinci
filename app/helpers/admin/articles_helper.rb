module Admin::ArticlesHelper

  ARTICLE_IMAGE_OPTIONS = {
    small: { width: 20, height: 20 },
    no_image: { width: 0, height: 0 },
    smallmid: { width: 578, height: 200, class: "smallmid-image" },
    mix: { width: 473, height: 200 },
    medium: { width: 968, height: 350, class: "medium-image"},
    large: { width: 1920, height: 500, class: "large-image" }
  }

  def article_image_tag(article, size)
    image_path = article_image_path(article, size)
    options = ARTICLE_IMAGE_OPTIONS[size]
    cl_image_tag(image_path, options)
  end

  def article_image_path(article, size)
    if article.image.present?
      article.image.path
    else
      nil
    end
  end

  def article_date(article)
    if article.published_on
      article.published_on.strftime("%d-%m-%Y")
    else
      "-"
    end
  end

end
