module Admin::EventsHelper
  EVENTS_IMAGE_HELPER = {
      small: { width: 20, height: 20 },
      medium: { width: 70, height: 70, class: "event-logo"},
      large: { width: 500, height: 300 }
    }

  def events_image_tag(event, size)
    image_path = event_image_path(event, size)
    options = EVENTS_IMAGE_HELPER[size]
    cl_image_tag(image_path, options)
  end

  def event_image_path(event, size)
    if event.home.present?
      event.home.path
    else
      nil
    end
  end

  def events_away_tag(event, size)
    image_path = event_away_path(event, size)
    options = EVENTS_IMAGE_HELPER[size]
    cl_image_tag(image_path, options)
  end

  def event_away_path(event, size)
    if event.away.present?
      event.away.path
    else
      nil
    end
  end
end
