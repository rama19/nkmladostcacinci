module Admin::BaseHelper

  def human_name_for_boolean(boolean)
    boolean ? "DA" : "NE"
  end

  def admin_menu_items
    [
      ["Početna", admin_root_path],
      ["Početna - Web", root_path]
    ]
  end

  def render_admin_menu
    items = admin_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      link = link_to(item[0], item[1])
      content_tag(:li, link, class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "right hide-on-med-and-down")
  end
end
