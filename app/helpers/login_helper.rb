module LoginHelper

  def current_user_initials
    "#{current_user.first_name.first}#{current_user.last_name.first}".upcase
  end

end
