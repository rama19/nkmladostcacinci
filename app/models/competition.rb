class Competition < ActiveRecord::Base
  has_many :events
  has_many :championships

  validates :name, presence: true

end
