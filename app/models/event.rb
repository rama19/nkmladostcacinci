class Event < ActiveRecord::Base
  belongs_to :competition
  belongs_to :category

  has_attachment :home, accept: [:jpg, :jpeg, :gif, :png]
  has_attachment :away, accept: [:jpg, :jpeg, :gif, :png]

  validates :competition, presence: true
  validates :matchday, presence: true
  validates :place, presence: true
  validates :game_date, presence: true
  validates :game_time, presence: true
  validates :home_team, presence: true
  validates :away_team, presence: true
end
