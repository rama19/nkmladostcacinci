class Championship < ActiveRecord::Base
  belongs_to :category

  validates :team, presence: true
  validates :goal_difference, presence: true

end
