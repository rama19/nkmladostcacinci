$(document).ready(function() {
  $('.modal-trigger').leanModal({
    complete: function () {
      $('.lean-overlay').remove();
    }
  });
  $('select').material_select();
  $('.datepicker').pickadate({
    selectMonths: true,
    selectYears: 1
  });
});

