$ ->
  # Image upload via attachinary
  $('.attachinary-input').attachinary()

  # Images
  $pageBodyTextarea = $("#page_body")
  if $pageBodyTextarea.length > 0
    $newImageModal = $("#new_image_modal")
    $imagesModal = $("#images-modal")

    # Add (and use) new image
    $("form.new_image")
      .on("ajax:success", (event, data) ->
        $newImageModal.modal("hide")
        $pageBodyTextarea[0].insertAtCaret("<img src=\"#{data.image_url}\" class=\"img-responsive\">")
        $pageBodyTextarea.focus()
      )
      .on("ajax:error", (event, xhr) ->
        response = xhr.responseJSON
        alert("ERROR (#{xhr.status}): #{response.errors}")
      )

    # Clean previously uploaded images
    $newImageModal
      .on("shown.bs.modal", ->
        $(".attachinary_container a[data-remove]").trigger("click")
      )

    # Image gallery
    $imagesModal
      .on("shown.bs.modal", ->
        path = $(@).data("images-path")
        $.getJSON(path, (data) ->
          $imagesModal.find(".modal-content").html(data.html)
        )
      )
      .on("click", "button.choose-image", ->
        $button = $(@)
        imageUrl = $button.parents(".thumbnail").find("a").attr("href")
        $imagesModal.modal("hide")
        $pageBodyTextarea[0].insertAtCaret("<img src=\"#{imageUrl}\" class=\"img-responsive\">")
        $pageBodyTextarea.focus()
      )

    # Pagination
    $imagesModal.on("click", ".pagination a", (event) ->
      event.preventDefault()
      $link = $(@)
      $.getJSON($link.attr("href"), (data) ->
        $imagesModal.find(".modal-content").html(data.html)
      )
    )



