Rails.application.routes.draw do
  mount Attachinary::Engine => "/attachinary"

  root to: "site#home"

  get "site/:slug" => "site#show", as: :show_article

  %w(blog
    history).each do |page|
    get page, to: "site##{page}", as: page
  end

  scope :team do
    %w(team juniors pionirs u12 u10 u8).each do |page|
      get page, to: "team##{page}", as: page
    end
  end

  namespace :admin do
    root to: "interface#index"

    resource :session, controller: 'clearance/sessions', only: [:create]
    get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
    delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'


    resources :interface, only: [:index]

    resources :articles
    resources :images, only: [:index, :create]

    resources :competitions
    resources :events
    resources :categories
    resources :championships

  end
end
